! function(t) {
	function e(t, e, a, n) {
		"undefined" != typeof e.errors && e.errors.length > 0 ? t.reject(e.errors, e.data) : "undefined" != typeof e.data ? (null !== e.data && "undefined" != typeof e.data.fail && !0 === e.data.fail && t.reject(!1), null === e.data ? t.resolve(!0) : t.resolve(e.data)) : "undefined" != typeof e.result ? null === e.result ? t.resolve(!0) : t.resolve(e.result) : "undefined" != typeof e.fail && !1 === e.fail ? t.reject(!1) : t.resolve(!1)
	}

	function a(a, n, i, r, o) {
		var l = new t.Deferred,
			d = {
				ajaxUrl: "/ajax/::controller::.php?action=::action::",
				v2: {
					ajaxUrl: "/json/::controller::/::action::"
				}
			};
		return "undefined" == typeof r && (r = !1), "undefined" == typeof o && (o = r ? d.v2.ajaxUrl : d.ajaxUrl), this.latestXhr = o = o.replace("::controller::", a).replace("::action::", n), "undefined" == typeof i && (i = {}), t.ajax({
			type: "POST",
			url: o,
			data: i
		}).done(function(t, a, n) {
			e(l, t, a, n)
		}).fail(function(t, e, a) {
			l.reject(t, e, a)
		}), l.promise()
	}

	function n(t) {
		for (var e = "ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz", a = t || 8, n = "", i = 0, r = 0, o = 0, l = 0; a > l; l++) 0 === Math.floor(2 * Math.random()) && 3 > r || i >= 5 ? (o = Math.floor(10 * Math.random()), n += o, r += 1) : (o = Math.floor(Math.random() * e.length), n += e.substring(o, o + 1), i += 1);
		return n
	}

	function i() {
		var t = navigator.userAgent.toLowerCase();
		return -1 != t.indexOf("msie") ? parseInt(t.split("msie")[1]) : !1
	}

	function r() {
		var e = this,
			a = t('[data-field="geboren.day"]', this.cnt),
			n = t('[data-field="geboren.month"]', this.cnt),
			r = t('[data-field="geboren.year"]', this.cnt),
			h = (new Date, null),
			f = !1;
		for (h = 18; 100 > h; h++) t("[data-age]", this.cnt).append('<option value="' + h + '">' + h + "</option>");
		t('[data-register="confirm"]', this.cnt).click(function(a) {
			var n = t(this);
			a.preventDefault(), n.hasClass("disabled") || (n.addClass("disabled"), c.call(e, !0).then(function() {
				n.removeClass("disabled"), t("[data-form]", e.cnt).hide(), t("[data-confirm]", e.cnt).show(), t("[data-givenname]", e.cnt).text(t('[data-field="clientname"]', e.cnt).val()), t("[data-givenpass]", "[data-confirm]").text(t('[data-field="passw"]', e.cnt).val()), t("[data-givenemail]", "[data-confirm]").text(t('[data-field="email"]', e.cnt).val()).attr("title", t('[data-field="email"]', e.cnt).val())
			}, function() {
				n.removeClass("disabled")
			}))
		}), t('[data-register="submit"]', this.cnt).on("click", function(a) {
			var n = t(this);
			n.hasClass("disabled") || (n.addClass("disabled"), c.call(e).then(function() {
				n.removeClass("disabled")
			}, function() {
				n.removeClass("disabled")
			}))
		}), t("[data-next]", this.cnt).click(function() {
			t('[data-cnt="error"]', e.cnt).html(""), t(this).closest("[data-step]").hide().next("[data-step]").show()
		}), t("[data-prev]", this.cnt).click(function() {
			t('[data-cnt="error"]', e.cnt).html(""), t(this).closest("[data-step]").hide().prev("[data-step]").show()
		}), t("[data-field]", this.cnt).change(function() {
			var e = t(this).closest("[data-step]");
			s(e)
		}), t("input", this.cnt).keyup(function() {
			t(this).trigger("change")
		}), t('[data-field="geboren.day"], [data-field="geboren.month"], [data-field="geboren.year"]', this.cnt).on("change", function(e) {
			if (f !== !0) {
				var o = parseInt(r.val()),
					l = parseInt(n.val()),
					d = parseInt(a.val()),
					c = null,
					u = 12,
					p = (t("select", t(this).closest("[data-step]")).filter(function() {
						return "" === t(this).val()
					}), t(this).closest("[data-step]"));
				if (f = !0, void 0 !== o)
					for (o === (new Date).getFullYear() - 18 && (u = (new Date).getMonth() + 1, l === u && (c = (new Date).getDate())), h = 1; 12 >= h; h++) h > u ? i() === !1 ? t('option[value="' + h + '"]', n).prop("disabled", !0) : t('option[value="' + h + '"]', n).hide() : i() === !1 ? t('option[value="' + h + '"]', n).prop("disabled", !1) : t('option[value="' + h + '"]', n).show();
				if (l > u && (n.val(""), s(p)), null !== c || void 0 !== o && void 0 !== l)
					for (null === c && (c = new Date(o, l, 0).getDate()), void 0 !== d && d > c && (a.val(""), s(p)), h = 1; 31 >= h; h++) h > c ? i() === !1 ? t('option[value="' + h + '"]', a).prop("disabled", !0) : t('option[value="' + h + '"]', a).hide() : i() === !1 ? t('option[value="' + h + '"]', a).prop("disabled", !1) : t('option[value="' + h + '"]', a).show();
				f = !1
			}
		}), t('[data-age="min"]', this.cnt).change(function() {
			var e = t(this).val(),
				a = !1;
			t("option", '[data-age="max"]').each(function() {
				parseInt(t(this).attr("value")) < e ? (t(this).attr("disabled", "disabled"), t(this).is(":selected") && (a = !0)) : t(this).removeAttr("disabled")
			}), a && t("option", '[data-age="max"]').first().attr("selected", "selected")
		}), t('[data-age="max"]', this.cnt).change(function() {
			var e = t(this).val(),
				a = !1;
			return "" === e ? void t("option", '[data-age="min"]').removeAttr("disabled") : (t("option", '[data-age="min"]').each(function() {
				parseInt(t(this).attr("value")) > e ? (t(this).attr("disabled", "disabled"), t(this).is(":selected") && (a = !0)) : t(this).removeAttr("disabled")
			}), void(a && t("option", '[data-age="min"]').first().attr("selected", "selected")))
		}), t('[data-action="searchCity"]', this.cnt).on("keyup", function(a) {
			var n = t(this).val();
			n.length < 3 || n != e.lastCityPart && (e.lastCityPart = n, o(e, n))
		}), t("[data-search]").click(function() {
			var e = null,
				a = null;
			e = t('[data-field="region1"]').val(), a = t('[data-field="region2"]').val(), "England" != e && (a = e);
			var n = {
				conditions: {
					eq: {
						gender: "male" === t('[data-field="gender"]').val() ? "female" : "male",
						country: e,
						region: a,
						hasPhoto: 1
					},
					gte: {
						age: t('[data-age="min"]').val()
					},
					lte: {
						age: t('[data-age="max"]').val()
					},
					limit: 24
				}
			};
			l(n).then(function(e) {
				24 > e ? (delete n.conditions.eq.woonplaats, l(n).then(function(e) {
					24 > e ? (delete n.conditions.eq.woongebied, l(n).then(function(e) {
						24 > e && t("[data-seeall]").hide(), d(e, n)
					})) : l(n).then(function(t) {
						d(t, n)
					})
				})) : l(n).then(function(t) {
					d(t, n)
				})
			}), t("h1").hide()
		})
	}

	function o(e, n) {
		var i = e,
			r = {};
		r.cityPart = n, "object" == typeof i.loadCityXhr && null !== i.loadCityXhr && i.loadCityXhr.abort(), a("common", "autoSuggestCity", r).then(function(e) {
			var a = t("[data-result='searchCity']"),
				n = t("[data-cnt='searchCity']");
			a.html(""), n.show(), e === !1 && ($li = t("<li>").append("<p>No city information found</p>")), $li = t("<li>").attr("data-region1", "Australia").attr("data-region2", "Victoria").attr("data-town", "Melbourne city").append("<p><span class='mapmarker'></span> Melbourne <span class='region'> - (Queensland)</span></p>"), a.append($li), $li.on("click", function() {
				t('[data-field="region1"]').val(t(this).data("region1")), t('[data-field="region2"]').val(t(this).data("region2")), t('[data-field="town"]').val(t(this).data("town")), t('[data-field="localityField"]').val(t(this).data("town")), t('[data-field="location"]').val(t('[data-field="region1"]').val() + "-" + t('[data-field="region2"]').val() + "-" + t('[data-field="town"]').val()), t('[data-field="town"]').trigger("change"), n.hide(), t("[data-place]").text(t(this).data("town"))
			})
		}, function(t) {}), i.loadCityXhr = this.latestXhr
	}

	function l(e) {
		var n = t.Deferred();
		return a("profile", "count", e, !0).then(function(t) {
			n.resolve(t)
		}), n.promise()
	}

	function d(e, n) {
		a("profile", "search", n, !0).then(function(a) {
			t("[data-total]").text(e);
			for (var n in a) t("[data-results]").append('<li><img src="/content/' + a[n].clientpath + '/splash.jpg"></li> ')
		}, function(t) {})
	}

	function s(e) {
		var a = 0;
		return t("[required]", e).each(function() {
			"" === t(this).val() && a++
		}), a > 0 ? (t("[data-next]", e).hide(), !1) : void t("[data-next]", e).show()
	}

	function c(e) {
		function a(t, e) {
			var a = null,
				n = null,
				i = l;
			for (a = t.split("."), n = 0; n < a.length - 1; n++) void 0 === i[a[n]] && (i[a[n]] = {}), i = i[a[n]];
			i[a[n]] = e
		}

		function i(e) {
			var a = null,
				n = t('[data-cnt="error"]', o.cnt),
				i = '<ul class="error">';
			for (a = 0; a < e.length; a++) "undefined" != typeof o.options.errors[e[a]] && (i += "<li>" + o.options.errors[e[a]] + "</li>");
			o.cnt.addClass("has-errors"), n.html(i).show(), 1 === e.length && r(e[0])
		}

		function r(e) {
			var a = null;
			switch (e) {
				case "accept_conditions":
					return;
				case "clientname_too_long":
				case "disallowed_characters":
				case "clientname_required":
				case "clientid_unavailable":
					a = t('[data-field="clientname"]', o.cnt), t("[data-confirm]", o.cnt).hide(), a.closest("[data-step]").show();
					break;
				case "email_invalid":
				case "email_unavailable":
				case "email_required":
					a = t('[data-field="email"]', o.cnt), t("[data-confirm]", o.cnt).hide(), a.closest("[data-step]").show();
					break;
				case "password_too_long":
				case "password_required":
					a = t('[data-field="passw"]', o.cnt), t("[data-confirm]", o.cnt).hide(), a.closest("[data-step]").show()
			}
			t('[data-register="confirm"]', "[data-form]").remove(), t('[data-register="submit"]', "[data-form]").show()
		}
		var o = this,
			l = {},
			d = [],
			s = new t.Deferred;
		return this.cnt.removeClass("has-error"), t('[data-cnt="error"]', o.cnt).html(""), t("[data-field]", this.cnt).each(function() {
			var e = t(this);
			return !e.attr("required") || null !== e.val() && "" !== e.val() ? "checkbox" === e.attr("type") && e.prop("checked") === !1 ? void d.push(e.data("field") + "_not_checked") : void a(e.data("field"), e.val()) : void d.push(e.data("field") + "_empty")
		}), d.length > 0 ? (i(d), s.reject(), s.promise()) : (l.dateOfBirth = l.geboren.year + "-" + f(l.geboren.month, 2, "0") + "-" + f(l.geboren.day, 2, "0"), t("[data-conditions]").length > 0 ? l["conditions-accepted"] = t("[data-conditions]").is(":checked") : l["conditions-accepted"] = !0, 0 === t('[data-field="passw"]').length && (l.passw = n(8)), l.search = {
			gender: t('[data-match="gender"]', this.cnt).val(),
			minAge: t('[data-age="min"]', this.cnt).val(),
			maxAge: t('[data-age="max"]', this.cnt).val(),
			country: t('[data-field="woongebied"]', this.cnt).find(":selected").closest("optgroup").attr("label"),
			region: l.woongebied
		}, e === !0 ? s.resolve() : s.promise())
	}

	function h(t, e, a, n) {
		var i = t.toString(),
			r = null,
			o = e - i.length,
			l = function(t, e) {
				for (var a = ""; a.length < e;) a += t;
				return a = a.substr(0, e)
			};
		if (void 0 === e || 0 >= e) return i;
		if (0 > o) return i;
		switch (a = a || " ", n) {
			case "STR_PAD_LEFT":
				i = l(a, o) + i;
				break;
			case "STR_PAD_BOTH":
				r = l(a, Math.ceil(o / 2)), i = r + i + r;
				break;
			case "STR_PAD_RIGHT":
			default:
				i += l(a, o)
		}
		return i
	}

	function f(t, e, a) {
		return h(t, e, a, "STR_PAD_LEFT")
	}
	var u = "app.register",
		p = t.fn.register,
		v = function(e, a) {
			this.cnt = t(e), this.latestXhr = null, this.loadCityXhr = null, this.options = a, r.call(this)
		};
	v.defaults = {
		errors: {
			clientname_empty: "Please enter a username.",
			passw_empty: "Please enter a password.",
			"geboren.day_empty": "Please enter a birth day.",
			"geboren.month_empty": "Please enter a birth month.",
			"geboren.year_empty": "Please enter a birth year.",
			email_empty: "Please enter an email address."
		}
	}, t.fn.register = function(e) {
		var a = arguments,
			n = this.each(function() {
				var n = t(this),
					i = null,
					r = null,
					o = [];
				if (i = n.data(u), r = t.extend(!0, {}, v.defaults, n.data(), e), void 0 === i && n.data(u, i = new v(this, r)), "string" == typeof e) {
					if (void 0 === i[e]) throw "Method " + e + " does not exists";
					return Array.prototype.push.apply(o, a), o.shift(), i[e].apply(i, o)
				}
			});
		return n
	}, t.fn.register.Constructor = v, t.fn.register.noConflict = function() {
		return t.fn.register = p, this
	}
}(jQuery, window);